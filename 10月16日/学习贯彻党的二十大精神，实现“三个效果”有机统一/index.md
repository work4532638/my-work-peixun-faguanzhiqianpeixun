# 学习贯彻党的二十大精神，实现“三个效果”有机统一

## 概述

习近平法治思想：必须坚定不移走中国特色社会主义政治发展道路，坚持党的领导、人民当家作主、依法治国有机统一。

习近平总书记：“坚持以法为据、以理服人、以情感人，努力实现最佳的法律效果、政治效果、社会效果。”  ——习近平：《论坚持全面依法治国》，中央文献出版社2020年版，第259-260页。
 
习近平总书记:“法律不应该是冷冰冰的，司法工作也是群众工作。一纸判决，或者能够给当事人正义，却不一定能解开当事人的‘心结’，‘心结’没有解开，案件也就没有真正了结。”  ——习近平：《论坚持全面依法治国》，中央文献出版社2020年版，第23页。

理念上的风险：单一思维、个案思维、学院思维、脱法思维。

实践中的案件：“陆勇案”、“赵春华案”、“电梯劝阻吸烟案”等等。

## 一、“三个效果”概念辨析及其内在逻辑

### “三个效果”是什么？

法律效果：严格适用和执行法律规定达到的作用和效果。1．法律适用的强制性; 2．法律适用的统一性，即实现“类案同判”; 3．司法活动的程序正义。

政治效果：1.法律适用对于保障政治秩序、政治安全所要达到的效果。---→司法活动贯彻落实了总体国家安全观。2.自由裁量权之内的政策落实；3.司法延伸功能的发挥。

社会效果：司法活动取得社会公众对司法过程和结论的认同，司法裁判符合普通人的价值观和朴素正义观 。

### “三个效果”有机统一的必要性

1. 践行新时代能动司法的必然要求。

2. 弥补成文法的不足
    1. 规则空白、规则冲突、规则滞后、规则歧义等
    1. 经济社会的快速发展带来的新问题
3. 弘扬中华优秀法律文化
    1. 习近平法治思想与中华优秀法律文化的内在联系。
        1. 习近平总书记：“我国古代法制蕴含着十分丰富的智慧和资源，中华法系在世界几大法系中独树一帜。要注意研究我国古代法制传统和成败得失，挖掘和传承中华法律文化精华，汲取营养、择善而用。”  ——习近平：《论坚持全面依法治国》，中央文献出版社2020年版，第110-111页。
        1. “同居相为隐”、“矜恤老幼”、“五服定罪”、“存留养亲”
        1. “秋审初分为情实、缓决、矜、疑，然疑律不经见。雍正之后，加入留养承祀，区分为五类”
        1. 张晋藩：“法合人情则兴，法逆人情则竭。情入于法，使法与伦理相结合，易于为人所接受；法顺人情，冲淡了法的僵硬与冷酷的外貌，更易于推行。”
        1. “每听一事，须于堂下稠人广众中，择傅老成数人，体问风俗，然后折衷剖断，自然情理兼到。”——汪辉祖：《学治臆说·初任须体问风俗》，载郭成伟：《官箴书点评与官箴文化研究》，中国法制出版社2000年版，第203页。
        1. 律令决狱与引经决狱（春秋决狱）的关系。
4. 国际法学理论的批判吸收
    1. 注释法学、概念法学
    
        哈佛法学院长兰代尔：“法律（司法）是一门科学”
    1. 现实主义法学：霍姆斯、卢埃林、卡多佐。

### “三个效果”发生冲突的情形

司法活动中的“三个效果”通常不发生冲突 。但是：
1. 不当适用法律，导致“三个效果”皆失，例如“公平责任”的滥用。例如只讲具体条文不讲立法目的和立法精神。
2. “所谓”准确适用法律，但是社会经济生活已经发生了重大变化，“法律效果”与“社会效果”之间产生冲突，例如清算案件继续适用《公司法司法解释二》第18条，要求所有中小股东承担清算义务人民事责任的判决。
3. 法律存在规则空白、规则冲突、规则歧义等情形，法律解释方法不统一，类案不能同判，导致了“三个效果”皆失。
4. 正确适用了法律，但未能发现案件中的政治意义，就案判案，将司法技术置于绝对地位，导致判后出现意识形态风险或者涉众聚集等，“法律效果”与“政治效果”产生冲突。
5. 法律授予自由裁量权，但未能斟酌案情细微差别，未能妥善行使裁量权，“社会效果”不佳。
6. 程序与实体均为妥当，但文书表达出现纰漏或者引起重大误解，“三个效果”出现冲突。
7. 司法行为不规范不善意，缺乏司法礼仪和人文关怀 。
8. 司法延伸功能发挥不足：司法引导、司法预警不够，社会效果欠缺。（预警员、引导员、参谋员、审判员）
9. 不敢、不愿、不屑行使释明权。
10. 裁判文书释法说理不清、不到位。

### “三个效果”的内在逻辑关系

1. 法律效果是前提和基础。
2. 社会效果是内在要求。
3. 政治效果是最高价值追求。

## 二、政治效果及其实现途径

【案例】

法院作出的一份离婚判决中，法官引用《圣经》进行说理：“为什么看到你弟兄眼中有刺，却不想自己眼中有梁木呢。你自己眼中有梁木，怎能对你兄弟说，容我去掉你眼中的刺呢。你这假冒伪善的人，先去掉自己眼中的梁木，然后才能看得清楚，以去掉你兄弟眼中的刺。——《圣经·马太福音》。正人先正己。人在追求美好婚姻生活的同时，要多看到自身的缺点和不足，才不至于觉得自己完全正确。”

问题：宗教典籍、宗教语言与习惯引入判决文书，如何看待？

习近平总书记：处理我国宗教关系，必须牢牢把握坚持党的领导，巩固党的执政地位、强化党的执政基础这个根本，必须坚持政教分离，坚持宗教不得干预行政、司法、教育等国家职能实施。——《论坚持党对一切工作的领导》，中央文献出版社，2019年版，第140页。

### 树牢政治机关意识

习近平总书记：“全面推进依法治国必须走对路。要从中国国情和实际出发，走适合自己的法治道路，决不能照搬别国模式和做法，决不能走西方“宪政”、“三权鼎立”、“司法独立”的路子。”
 
 西方“司法独立”的内涵: 
1. 国体与政体意义上的“司法独立”

司法权、立法权、行政权三足鼎立，司法权独立于立法权和行政权，不受立法权和行政权的任何干预和约束。“司法独立”是西方宪政的重要组成部分。
2. 政党意义上的“司法独立”
    司法不受任何政党的监督和管理，即所谓“政治中立原则。”
3. 机构意义上的“司法独立”
    西方“司法独立”理论通常将司法独立分为法院独立、法官独立。
4. 裁判意义上的“司法独立”
    裁判意义上的司法独立，也称之为法官独立，是指在审判案件中审判权完全独立，不受任何人指挥和命令的拘束。
5. 保障“司法独立”的配套制度
    西方“司法独立”理论认为，“司法独立”需要依靠一些特定的配套制度才能得以实现：包括法院对立法与行政机关的制衡权，例如法院的违宪审查权、法官任期、固定和充足的薪酬、司法豁免以及人身安全保护等。

西方“司法独立”的破产:

美国中佛罗里达大学拉尼尔教授（Drew Noble Lanier）和汉德伯格教授（Roger Handberg）：“司法独立这个概念经常被绝对使用，但现实很复杂。无论对法院如何修饰，法院就是政治机构，源于政治并服务于政治。”

美国圣塔克拉拉大学帕雷迪教授（Terri Peretti）：“我确定这是一个事实——法官，即使是联邦最高法院的大法官，并没有完全的独立，这是最没有争议的事实。”

1. 法官任命制与西方“司法独立”的破产
2. 法官选举制与西方“司法独立”的破产
3. 干涉司法判决与西方“司法独立”的破产
4. 法官弹劾制度与西方“司法独立”的破产
5. 行政法官制度与西方“司法独立”的破产
6. 保障“司法独立”的制度失灵与西方“司法独立”的破产

Shirley S. Abrahamson：“当法官是依靠选区的大多数人来赢得职位并继续留任，那么这些法官如何才能对选区的少数派利益保持公正审判呢？当法官是依靠某些团体甚至是律师来募集资金进行竞选时，法官如何保持公正？”

1. 我们反对的不是独立公正司法，反对的不是司法机关“依法独立公正行使司法权，”恰恰相反，我们要加强独立公正司法，要严格落实“三个规定。”
1. 我们所反对的是，鼓吹“司法独立”来取消党的领导，改变中国的根本政治制度；反对的是把党的领导和独立司法对立起来，借机制造理论和思想的混乱。
1. 党的二十大报告: 坚持依法治国首先要坚持依宪治国，坚持依法执政首先要坚持依宪执政，坚持宪法确定的中国共产党领导地位不动摇，坚持宪法确定的人民民主专政的国体和人民代表大会制度的政体不动摇。

### 妥善处理涉及政治安全、经济安全、社会安全等方面的个案

国家安全、社会主义主义核心价值观、涉民族宗教、涉众案件、涉民生类、涉公共安全类案件等

习近平总书记：“西方国家策划“颜色革命”，往往从所针对的国家的政治制度特别是政党制度开始发难，大造舆论，大肆渲染，把不同于他们的政治制度和政党制度打入另类，煽动民众搞街头政治。”

“颜色革命”的套路： 矛盾积累——个案点燃——网络舆情——内外勾结——区域爆发

### 落实从“从政治上看，从法治上办”

资本主义国家的司法机关服务资本主义国家的政治

1824年美国联邦最高法院审理的吉本斯诉奥格登案

1904年的北方证券公司诉美国案

习近平总书记：“法治当中有政治，没有脱离政治的法治。每一种法治形态背后都有一套政治理论，每一种法治模式背后都有一种政治逻辑，每一条法治道路底下都有一种政治立场。”

1. 要善于“从政治上看”，善于透过表象看到案件、事件、问题背后的政治因素和影响。
1. 要善于“从法治上办”，对于政治性很强的案件，要善于通过司法手段使敏感问题脱敏，用讲法治的方式办好这些案件 。

### 确保司法活动符合党的政策目标并有力嵌入社会治理
1. 法律衡量加上政策衡量
贯彻落实党的基本政策，服务党和国家工作大局，这是“政治效果”的应有之义。

例如破产案件处理与“绿色”发展理念，金融案件处理与重大风险防范，公司案件处理与国企现代化，执行工作与中小微企业抒困等，均存在司法活动与政策目标的内在关联 。

2. 司法嵌入国家社会治理
习近平总书记：法治建设既要抓末端、治已病，更要抓前端、治未病。

司法引导、司法预警、司法服务、司法教育、司法建议等，例如企业合规的问题。例如金融审判：人民法院的“预警员”“引导员”“参谋员”的作用。

### 把握裁判文书写作中的政治效果
【案例】

判决书上写明了法律适用理由——“《种子法》实施后，玉米种子的价格已由市场调节，《河南省农作物种子管理条例》作为法律阶位较低的地方性法规，其与《种子法》相冲突的条款自然无效……”

问题：如何看待本案的文书表达？

## 三、法律效果及其实现途径

### 熟练掌握裁判方法
形式主义法学裁判方法与现实主义法学裁判方法

以民事审判为例：
1. 法律关系分析法：杨立新、王利明等
2. 请求权基础法（归入法或涵摄法）：王泽鉴步骤：
    1. 确定请求权（确认或形成之诉，不能用本方法）
    1. 确定请求权的性质（人身还是财产？）
    1. 确定请求权的基础（法律规范是什么）
    1. 对请求权基础进行分析（法律规范所要求的要件事实）
    1. 归入（对比考察）
    1. 作出裁判

【案例】“带孙费”纠纷

70多岁的老人张某某一纸诉状将儿子和儿子的前妻告上法庭，要求支付其带孙女所垫付的抚养费28.8万元。张某某之所以诉请支付“带孙费”，是因为自从赵某出生后，“两被告就将孩子给原告抚养和照顾，对孩子不闻不问，更别说支付任何费用，对孩子的抚养费也只字不提”。张某某在诉状中称，“现在孩子都16岁了，但被告认为原告抚养孙女是理所当然的事，对原告的态度极其冷漠，在原告带孙女忙碌之时，被告不但不心存感激，反而嘲讽原告，让原告伤心至极”。

【案例】

买卖合同等案件中，被告为一人有限责任公司及股东。原告未提供一人公司股东财产与公司财产混同的初步证据，被告一人公司股东也未提供股东财产独立于公司财产的证据。是否一律依据《公司法》第六十三条规定，判决股东对一人公司债务承担连带责任。

请求权？请求权基础？请求权基础要件？要件事实？

规范出发型：
1. 庭审前阶段
    1. 诉请 + 事实理由
        1. 特定诉讼标的
            1. 检索基础规范
                1. 初步分解要件
1. 庭审中阶段
    1. 围绕要件事实
        1. 当事人之间
            1. 主张和举证
                1. 促成法官心证
        1. 法官之间
            1. 引导诉讼进程
                1. 确保公正效率
1. 庭审后阶段
    1. 围绕诉讼标的
        1. 法官心证 + 证明责任
            1. 作出裁判
                1. 形成既判力



现实主义法学裁判方法：

1. 法官卡多佐：我的裁判方法主要是逻辑、历史、习惯、社会学。
1. 霍姆斯：法律的生命不在于逻辑，而在于经验

现实主义法学裁判方法：
1. 考虑法律规则外因素：利益衡量、社会共识、伦理道德、社会政策、公众舆论、人情常理等，往往会超越规则的限制

1. 形式主义法学裁判方法————法律效果——形式逻辑
1. 现实主义法学裁判方法————社会效果——实质判断
1. 新时代中国特色社会主义裁判观———政治效果

### 正确进行法律解释与漏洞填补

黄茂荣教授：法律漏洞之发生的原因分为三种，立法者思虑不周，因法律上有意义之情况变更，立法者自觉对拟予规范之案型的了解还不够，而不加以规范。——黄茂荣：《法学方法与现代民法》（第五版），法律出版社2007年版，第427页。

例: 
1. 《民法典》第16条 涉及遗产继承、接受赠与等胎儿利益保护的，胎儿视为具有民事权利能力。但是，胎儿娩出时为死体的，其民事权利能力自始不存在。
1. 某甲出国期间，其他股东作出决议，对某甲除名。一年后某甲回国，起诉请求撤销除名决议。
1. 债权人基于《公司法》第20条第三款请求揭开公司面纱，要求股东背后的实际控制人承担连带责任。

### 实现类案同判

2020年9月，最高人民法院发布了《关于完善统一法律适用标准工作机制的意见》。

2021年12月1日实施的《最高人民法院统一法律适用工作办法》进一步明确了类案检索的情形和范围，明确了类案检索说明或报告的制作规范，强化类案检索制度要求，促进“类案同判。”

2022年1月最高人民法院成立统一法律适用工作领导小组。

### 彰显程序正义

诉讼程序中对当事人诉权、辩论权、陈述、知情权等诉讼权利不够尊重，诉讼程序推进过慢，缺乏应有的诉讼效率，就难以让人民群众感受到公平正义 。
1. 实体公正和程序公正、实质公正与形式公正的有机统一
1. 公正与效率的有机统一。
1. 近期负面舆情：与程序正义有关。

## 四、社会效果及其实现路径

政治判断力与法律专业判断力结合；法律专业判断力与群众朴素正义观结合。

司法过程中充分考虑法律规则外因素，包括利益衡量、社会共识、伦理道德、社会政策、人情常理等。

程序正义——实质正义——感受正义

现实主义法学裁判方法案例：
1. 无锡胚胎案中二审法官引入了伦理、情感、利益衡量三要素进行了考量；
1. 狼牙山五壮士名誉权案判决中，法官除了进行了形式主义的逻辑思维之外，还引入了社会主义核心价值观的考量；
1. 医生劝阻电梯吸烟案二审判决中，法官也引入了社会公共利益和社会主义核心价值观的考量

【案例】指导案例89 号（“北雁云依案”）

原告之妻产下一女取名“北雁云依”，并办理了出生证明和计划生育服务手册新生儿落户备查登记。为女儿办理户口登记时，被告济南市公安局历下区分局燕山派出所（以下简称“燕山派出所”）不予上户口。理由是孩子姓氏必须随父姓或母姓，即姓“吕”或姓“张”。根据《中华人民共和国婚姻法》（以下简称《婚姻法》）和《中华人民共和国民法通则》（以下简称《民法通则》）关于姓名权的规定，请求法院判令确认被告拒绝以“北雁云依”为姓名办理户口登记的行为违法。

裁判理由：
1. 首先，从社会管理和发展的角度，子女承袭父母姓氏有利于提高社会管理效率……
1. 其次，公民选取姓氏涉及公序良俗。在中华传统文化中，“姓名”中的“姓”，即姓氏，主要来源于客观上的承袭，系先祖所传，承载了对先祖的敬重、对家庭的热爱等，体现着血缘传承、伦理秩序和文化传统。而“名”则源于主观创造，为父母所授，承载了个人喜好、人格特征、长辈愿望等。
1. 社会效果应借助于法律现实主义裁判方法。例如为维护实质公正和具体正义的需要而加以适用、维护社会道德而加以适用、法律不明确需要加以解释时的适用等——例如：逻辑的结果不符合正义；例如法律存在漏洞；等等。
1. 法官的注意力从以往的概念、逻辑与三段论推理中转移到外部情境当中，比如对政策、社会效果的考量。让判决具有灵活性以及公正性。

### 利益衡量

[《关于深入推进社会主义核心价值观融入裁判文书释法说理的指导意见》法〔2021〕21号](./关于深入推进社会主义核心价值观融入裁判文书释法说理的指导意见.md)七。

[最高人民法院关于在审判执行工作中切实规范自由裁量权行使保障法律统一适用的指导意见 法发〔2012〕7号](./关于在审判执行工作中切实规范自由裁量权行使保障法律统一适用的指导意见.md)

七、正确运用利益衡量方法。行使自由裁量权，要综合考量案件所涉各种利益关系，对相互冲突的权利或利益进行权衡与取舍，正确处理好公共利益与个人利益、人身利益与财产利益、生存利益与商业利益的关系，保护合法利益，抑制非法利益，努力实现利益最大化、损害最小化。

胡云腾：《办案五断》：法官除了事实判断、法律判断、行为判断、案例判断之外，应该进行价值判断。“价值是司法裁判的灵魂和生命，”

权利与利益（《民法典》）:
1. 第1条 为了保护民事主体的合法权益…… 
1. 第3条 民事主体的人身权利、财产权利以及其他合法权益受法律保护，任何组织或者个人不得侵犯。
1. 第120条 民事权益受到侵害的，被侵权人有权请求侵权人承担侵权责任。
1. 第126条 民事主体享有法律规定的其他民事权利和利益。
1. 第1164条 本编调整因侵害民事权益产生的民事关系。

参照标准：
1. 利益位阶标准：生命（彼此无大小）→健康->财产（按照价值）；国家利益、社会公共利益、两造利益；特殊保护利益。
1. 社会主流价值观标准
1. 整体利益最大化或损害最小化标准
1. 社会正当激励标准

![利益综合图](利益综合图.jpg)

◼ 案例：
◼ 关于法定代表人涤除登记的四种情形:
1. 冒名登记要求涤除
2. 借名登记要求涤除
3. 恶意逃避责任要求涤除
4. 正常法定代表人要求涤除
### 伦理道德

习近平法治思想：坚持依法治国与以德治国相结合

法律与道德的关系:  “法律之内，应有天理人情在”

◼ 【案例】

李某依据父母八年前离婚协议约定内容，将房屋产权办在自己名下，并于当月诉至法院要求终止离婚后独自居住在此的母亲居住权。李某的父母经法院调解离婚。调解书中约定婚生子李某由父亲抚养，李某母亲一次性支付婚生子抚养教育费10000元至婚生子独立生活时止，双方共有的一房屋同意赠予婚生子李某，李某母亲在该房屋居住至李某年满18周岁。经查，离婚后，李某母亲独自居住在该房屋，李某与其父亲和爷爷奶奶共同生活，后父亲已经再婚。李某母亲现年42岁，原为工厂职工现无业，无其他房屋可供居住。

（三）社会共识
1. 【案例】“共同饮酒者担责的案件”
1. 【案例】“西客站行李箱案”
1. 【案例】“婚房自杀案”
1. 【案例】“醉驾送妻上医院案”
1. 【案例】“老人超市拿鸡蛋被拦猝死案 ”——法律框架：因果关系的必要性、相当性、可预见性。

在司法裁判中对“民意”展开对话式的回应，展现司法的“亲和力”、“民主性”。

习近平总书记：“对司法机关尚未或正在办理的案件，媒体可以报道，但不要连篇累牍发表应该怎么判、判多少年等评论，防止形成‘舆论审判’，以便为执法司法机关行使职权营造良好舆论环境。” ——习近平：《论坚持全面依法治国》，中央文献出版社2020年版，第53页。

### 公序良俗

[《全国民商事审判工作会议纪要》](./全国法院民商事审判工作会议纪要.md)31。

◼ 公序良俗适用的要求：
1. 适用上的一致性
1. 适用上的谦抑和谨慎。
1. 违背公序良俗无效的后果：返还？收缴？——（1）公法的效果；（2）侵害第三人的行为：返还；（3）既未违背公法又未侵害第三人利益的？

### 习惯、公共政策等

◼ 第十条 处理民事纠纷，应当依照法律；法律没有规定的，可以适用习惯，但是不得违背公序良俗。

◼ 功能：商事习惯的适用

◼ 【案例】公司财务会计账簿、会计凭证的保管纠纷（案情略）

### 司法行为与作风
法官应具有悲天悯人的情怀。所谓法官的“悲天悯人”，是指对国家和社会要有“先天下之忧而忧，后天下之乐而乐”的情怀，对基层人民群众要有深切的同情和关怀。时时刻刻对基层群众抱有同情心，甚至换位思考，把自己处于当事人的位置，才能做到“司法为民，”才能取得司法活动的“社会效果。”

### 把握“三个效果”有机统一的几个问题

### 裁判文书的法理情结合

“三个效果统一”，主要体现在裁判文书上，体现在裁判文书的情理法结合。裁判文书的说理性，就是把法中所蕴含的情理阐发出来。不能以情理替代法律、否定法律。法官应当选择大众公认的情理进行说理，并且情理必须是依托于法律。

释法说理不到位：三个效果受影响。

【案例】如何评判？

一例腾房纠纷的裁判文书：“一审法院判决202 室黄某所享有的份额折价归并给母亲虞某后，黄某未提出上诉，说明黄某心存善念。是的，母爱无私难忘，下面这首小诗道出了子女难以为报的心声：前天/ 我放学回家/ 锅里有一碗油盐饭。昨天/ 我放学回家/ 锅里没有一碗油盐饭。今天/ 我放学回家/ 炒了一碗油盐饭/ 放在妈妈的坟前！时间跑不过母亲的衰老，应是早搬为好。放下执念，便是晴天。与母亲和解，最终是为了与自己和解。让母亲心安，更让自己心安。母亲老了，黄某当感恩艰难岁月里母亲的坚持与付出，理解老年人的特殊需求，勿再设置任何程序性上的障碍。”

[《最高人民法院关于加强和规范裁判文书释法说理的指导意见》](最高人民法院关于加强和规范裁判文书释法说理的指导意见.md)二、十五、

### 以系统观念和辩证思维把握“三个效果”的关系

一是防止对“三个效果”有机统一存在空白认识。

二是防止对“三个效果”有机统一产生庸俗化的理解 。

既要防止就案论案，机械司法，又要防止自由裁量权的肆意，防止以“社会效果”“政治效果”的名义干预司法机关依法独立公正行使司法权，防止以“社会效果”“政治效果”的名义脱法裁判。

既要积极取得社会公众的认同，又要防止被狭隘的“民意”所裹挟。

既要追求与政策目标的一致性，又要防止以政策代替法律作出裁判。

### 掌握“三个效果”有机统一的裁判方法

既要熟悉形式理性，掌握形式主义法学的司法理念与方法，也要熟悉现实主义法学的司法理念与方法，要进行价值判断和实质理性考量

不能脱法裁判，不能机械司法。

### 实现“三个效果”有机统一思维方法
裁判思维：在行走之间穿越寻找规范
1. 行走于事实与规范之间；
2. 行走于规范与价值之间；
3. 行走于价值与社会条件之间；
4. 行走于实然规则与应然规则之间. 

——“眼光的往返流转”

在特定案件中，加强社会效果、政治效果的说理,下列案件的裁判文书，应当强化运用社会主义核心价值观释法说理：
1. 涉及国家利益、重大公共利益，社会广泛关注的案件；
1. 涉及疫情防控、抢险救灾、英烈保护、见义勇为、正当防卫、紧急避险、助人为乐等，可能引发社会道德评价的案件；
1. 涉及老年人、妇女、儿童、残疾人等弱势群体以及特殊群体保护，诉讼各方存在较大争议且可能引发社会广泛关注的案件；
1. 涉及公序良俗、风俗习惯、权利平等、民族宗教等，诉讼各方存在较大争议且可能引发社会广泛关注的案件；
1. 涉及新情况、新问题，需要对法律规定、司法政策等进行深入阐释，引领社会风尚、树立价值导向的案件；
1. 其他应当强化运用社会主义核心价值观释法说理的案件。

## 结 论

1. 中国法治：党的领导、人民当家作主、依法治国有机统一
1. 司法理念：“三个效果”有机统一
1. 裁判方法：三种方法，两个结合
1. 文书制作：法理情
1. 目标：以新时代能动司法践行公正与效率

## 补充

法官：
1. 孤独的人
1. 悲天悯人

习惯包括： 
1. 商事习惯
1. 固定的交易模式

公序良俗在民法典的 153条第二款

波斯纳法官？是一个学者型的法官？

醉驾的例子，带着妻子去医院，要放入法律框架，找的是 紧急避险的模式。

司法判决必须维护正常的社会交往模式

政治判断力和法律判断力

《利益衡量论》 梁教授 他还有一个表格

解释方法： 
1. 文义解释
1. 体系解释
1. 对象解释
1. 目的解释

总：
1. 利益衡量
1. 社会共识
1. 伦理道德
1. 社会政策
1. 公众舆论
1. 人情常理

程序正义 实质正义 感受正义

社会价值，主要是 主流的价值

滞后的政策如何解决： 
1. 法律原则
1. 最高院批复


1. 逻辑判断上做价值判断
1. 从政治上看，在法律上办

有人说，法律不在于逻辑，而在于经验。更在于法官对于当下时代的呼唤回应。

1. 做好预警员
1. 引导员
1. 参谋员: 司法建议



