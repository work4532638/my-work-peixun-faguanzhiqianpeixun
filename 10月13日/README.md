# 青年法官成长之路

1. 坚定理想信念是立身之本
    1. 理想信念坚定的时候，做的任何决定都会是正确的
    1. 
1. 勇于吃苦担当是成才的不竭动力
    1. 有解思维
    1. 案件体量大，工作量大，积累经验多
    1. 创造性的去落实
    1. 深度思考工作素养，写学术论文
    1. 珍惜平台，可能能够遇到机遇
1. 清正廉洁是成才的最稳定防线

## 办案捷径

判决一个案件，处理一类案由,需掌握：

1. 案例
1. 法律
1. 解释

## 祝福话语

小事不能小看，细节决定成败，要有小中见大的境界

急事不能急躁，每逢急事有静心，要有急中求静的素养

难事不能畏难，泰山压顶不弯腰，要有难中求胜的勇气

于高山之巅，方见大河奔涌，于群峰之上，更觉长风浩荡!

愿与各位一起不负时代、不负青春，勇立时代潮头，争做时代先锋，脚踏实地工作，向下扎根，向上生长，努力成为可用可造之才。
