##

赴最高人民法院办公一区、中国法院博物馆现场教学实施方案

一、指导思想及教学目标

坚持以习近平新时代中国特色社会主义思想为指导，全面贯彻落实党的二十大精神，深入贯彻落实习近平法治思想，通过现场教学提高教育培训质量效果，提升法官队伍司法能力和水平，加快推进审判体系和审判能力现代化。

二、教学地点

最高人民法院办公一区及中国法院博物馆

三、教学时间

2023年10月21日（周六）上午

四、教学对象

第二期全国法院法官职前培训班全体学员

五、教学安排 

本次现场教学以实地参观为主，参观地包括最高人民法院办公一区与中国法院博物馆，具体安排如下：

1. 因参观地场地所限，第二期全国法院法官职前培训班全体学员分为两批次开展现场教学活动。

（1）第一批次：刑事队（第一、三、五组，共72人）

该批次学员拟于早上8:10出发，约9:00抵达中国法院博物馆开始现场教学活动，参观时长为1小时左右，10:00再步行至最高人民法院办公一区继续参观。

（2）第二批次：民事队（第二、四、六组，共81人）

该批次学员拟于早上8:40出发，约9:30抵达最高人民法院办公一区并开始现场教学活动，参观时长为半小时左右，10:00再步行至中国法院博物馆继续参观。

（3）现场教学活动结束后，学员可自由活动，也可乘学院车返回学院。需乘车返回学院的学员，于最高人民法院办公一区门口集合，乘坐京B27707大巴车返回。

2.带队老师

（1）刑事队

刑事审判教研部司冰岩老师 电话：18813001510

（2）民事队

学员管理部刘承华老师     电话：13911571688

刑事审判教研部郭丰璐老师 电话：18810619060

3.本次现场教学交通出行由学院统一保障，总计四台大巴车辆。学院出发上车地点为：一号公寓楼南侧路上（地库出口）

（1）刑事队

目的地：中国法院博物馆（西门）

车号1：京B27567（单送）

车号2：京B13448（单送）

（2）民事队

目的地：最高人民法院办公一区

车号3：京B13545（单送）

车号4：京B27707（往返）

4.着装：请着便装，但不得太随意。

5.请随身携带身份证。


刑事审判教研部

郑未媚

司冰岩

郭丰璐

2023年10月20日
